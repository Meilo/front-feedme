var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var route = require('./route/route');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(express.static(path.join(__dirname)));

app.listen(3000);

app.use('/', route);
