angular.module("DirectiveCalendar", [])

.directive('onglet', [function(){
  return{
    restrict: "A",
    link: function(scope, iElement, attrs){
      angular.element(document).ready(function(){
        $('.tabs .tab-links a').on('click', function(e)  {
            var currentAttrValue = $(this).attr('href');

            // Show/Hide Tabs
            $('.tabs ' + currentAttrValue).show().siblings().hide();

            // Change/remove current tab to active
            $(this).parent('li').addClass('active').siblings().removeClass('active');

            e.preventDefault();
        });
      })
    }
  }
}])

.directive('lightbox',  ['$rootScope', '$timeout', '$window', function($rootScope, $timeout, $window) {
  return{
    restrict: "A",
    link: function(scope, iElement, attrs){
      if (scope.$last === true) {
        $timeout(function() {
            $(document).ready(function() {
              // if( $('#result-slideshow').length){
            	// 	$('#result-slideshow').bxSlider({
            	// 		pager: false,
            	// 		slideWidth: 350,
            	// 		maxSlides: 3
            	// 	});
            	// }
              $.each($rootScope.recettes, function(id, elem){
                  $('.open-popin-recette-'+elem.id).click(function(e){
                        e.preventDefault();
                     		$('#popin-add-meal').fadeOut();
                            $('#recette'+elem.id).css('z-index', '100');
                            $('#recette'+elem.id).css('opacity', '1');
                     		  $('.close-popin-recette-'+elem.id).click(function(){
                            $('.popin-recette-'+elem.id).css('z-index', '-5');
                            $('.popin-recette-'+elem.id).css('opacity', '0');
                            $('body').css('overflow','');
                     		   })
                           $('.add-to-meal').click(function(){
                             $('.popin-recette-'+elem.id).css('z-index', '-5');
                             $('.popin-recette-'+elem.id).css('opacity', '0');
                             $('body').css('overflow','');
                           })
                     	});
                  });
                  if($('#timepicker').length){
                  $('#timepicker').timepicker({
                     showMeridian: false
                    });
                   }
                  $.each($rootScope.recetteQuery, function(id, elem){
                      $('.open-popin-recette-'+elem.id).click(function(e){
                            e.preventDefault();
                         		$('#popin-add-meal').fadeOut();
                                $('#recette'+elem.id).css('z-index', '100');
                                $('#recette'+elem.id).css('opacity', '1');
                         		  $('.close-popin-recette-'+elem.id).click(function(){
                                $('.popin-recette-'+elem.id).css('z-index', '-5');
                                $('.popin-recette-'+elem.id).css('opacity', '0');
                                $('body').css('overflow','');
                         		   })
                               $('.add-to-meal').click(function(){
                                 $('.popin-recette-'+elem.id).css('z-index', '-5');
                                 $('.popin-recette-'+elem.id).css('opacity', '0');
                                 $('body').css('overflow','');
                               })
                         	});
                      });
                $(".chosen-select").chosen({
              		width:'500px'
              	});

                $.each($rootScope.repas, function(index, element){

                  $('.open-meal2-'+element.id).click(function(e){
                    e.preventDefault();
                    var thisOne = $(this);
                    $('.popin-add-meal2-'+element.id).fadeIn(function(){
                      var theDate = thisOne.closest('.day').data('date');
                      $('.add-meal-date-pick-'+element.id).val(theDate);
                    });
                  });
                  $('.close-add-meal2-'+element.id).click(function(e){
                    $('.popin-add-meal2-'+element.id).fadeOut();
                  });

                })

                $.each($rootScope.semaine, function(id, elem){

                  $(document).on("click", '.open-meal-'+elem, function(e){
                		e.preventDefault();
                		var thisOne = $(this);
                		$('.popin-add-meal-'+elem).fadeIn(function(){
                			var theDate = thisOne.closest('.day').data('date');
                			$('.add-meal-date-pick-'+elem).val(theDate);
                		});
                	});
                	$('.close-add-meal-'+elem).click(function(e){
                		$('.popin-add-meal-'+elem).fadeOut();
                	});
                	$.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
                	$('.add-meal-date-pick').datepicker({ 'dateFormat': 'yy-mm-dd' });
                	$('.add-meal-time-pick').timepicker({ 'timeFormat': 'H:i' });

                });

              });
      });
    }
  }
};
}]);
