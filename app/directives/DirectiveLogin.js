angular.module("DirectiveLogin", [])

.directive('toggler',  ['$rootScope', function($rootScope, $window) {
  return{
    restrict: "A",
    link: function(scope, iElement, attrs){
      angular.element('.toggle').on('click', function() {
        angular.element('.container').stop().addClass('active');
      });

      angular.element('.close').on('click', function() {
        angular.element('.container').stop().removeClass('active');
      });
    }
  }
}]);
