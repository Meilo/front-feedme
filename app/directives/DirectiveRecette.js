angular.module("DirectiveRecette", [])

.directive('choosen',  ['$rootScope', '$timeout', '$window', function($rootScope, $timeout, $window) {
  return{
    restrict: "A",
    link: function(scope, iElement, attrs){
      console.log(scope)
      if (scope.$last === true) {
        $timeout(function() {
          $(document).ready(function(){

          if( $(".chosen-select-step").length ){
            $(".chosen-select-step").chosen();
          }
          })
        })
      }
    }
  }
}])
.directive('selectation',  [function() {
  return{
    restrict: "A",
    link: function(scope, iElement, attrs){
      console.log(iElement)
      // if (scope.$last === true) {
      //   $timeout(function() {
          angular.element(document).ready(function(){
            // if($('select.simpleselect').length){
          	// 	$("select.simpleselect").simpleselect();
          	// }


            $('.recipe a.fav').click(function(e){
          		e.preventDefault();
          		var thisOne = $(this);
          		if(thisOne.hasClass('active')){
          			thisOne.removeClass('active');
          		}else{
          			thisOne.addClass('active');
          		}
          	});

            if($('textarea.tinymce').length){
          		tinymce.init({
          			selector: 'textarea.tinymce',
          			language: 'fr_FR'
          		});
          	}

          	if($('.preview-ingredient').length){
          		$("#select-from").change(function(){
          			$('.preview-ingredient').fadeIn();
          		});
          	}

          	if($('#step-two').length){
          		$('#btn-add').click(function(){
          		//	$('#select-from option:selected').each( function() {
              $('#showquantite').html(String($('#select-from option:selected').data('quantite')))
          				$('.popin-qte').fadeIn();
						$('body').css('overflow','hidden');
						var popinQte  = $('.popin-qte .qte-input');

						$('.popin-qte .qte-submit').click(function(){
							var popinQteVal = popinQte.val();
							if( popinQteVal != '' && !isNaN(popinQteVal) && parseInt(Number(popinQteVal)) == popinQteVal && !isNaN(parseInt(popinQteVal, 10)) ){
								$('.popin-qte').fadeOut();
								$('body').css('overflow','');
								$('#select-to').append("<option value='" + popinQteVal + ';' + String($('#select-from option:selected').data('idquantite')) + ';' + $('#select-from option:selected').val()+"' data-id='" + $('#select-from option:selected').val() + "' data-quantite='"+ String($('#select-from option:selected').data('quantite')) +"' data-nom='" + String($('#select-from option:selected').text().replace(/'/g, "&#39;")) + "'>"+ popinQteVal + " " + $('#select-from option:selected').text()+"</option>");
								$('#select-from option:selected').remove();




								popinQte.val('');
								$('#select-from option:selected,#select-to option:selected').removeAttr("selected");
							}
						});
          			});
          		//});
          		$('#btn-remove').click(function(){
          			$('#select-to option:selected').each( function() { var thisID = $(this).data('id'); var thisName = $(this).data('nom'); var thisQuantite = $(this).data('quantite')
          				$('#select-from').append("<option value='"+thisID+"' data-quantite='"+ thisQuantite +"' data-id='"+thisID+"' data-nom='"+String(thisName)+"'>"+thisName+"</option>"); console.log(thisName);
          				$(this).remove();
          			});
          		});
          	}

          	// if($('#step-three').length){
          	// 	$('#add-step-action').click(function(e){
          	// 		e.preventDefault();
            //
          	// 		var string = tinymce.get('mce_step').getContent();
          	// 		if(string != ''){
            //
          	// 			if( $('#upload-step-image').val() != ''  ){
            //
          	// 				var val = $('#upload-step-image').val();
          	// 				var valTest = val.substring(val.lastIndexOf('.') + 1).toLowerCase();
          	// 				var isImage = false;
          	// 				if( valTest == 'gif' || valTest ==  'jpg' || valTest ==  'png' ) isImage = true;
            //
          	// 				if(isImage){
            //
          	// 					$('#recipe-steps-content').append('<div class="the-step clearfix"></div>');
          	// 					$('#recipe-steps-content').find('.the-step:last-of-type').append('<div class="the-step-image"></div><div class="the-step-text"></div>');
          	// 					val = './img/poulet-salade.jpg';
          	// 					$('#upload-step-image').val('');
          	// 					$('#recipe-steps-content').find('.the-step:last-of-type .the-step-image').html('<img src="' + val + '">');
            //
          	// 				}else{
          	// 					$('p.error.error-image').fadeIn();
          	// 					return;
          	// 				}
            //
          	// 			}else{
            //
          	// 				$('#recipe-steps-content').append('<div class="the-step clearfix"></div>');
          	// 				$('#recipe-steps-content').find('.the-step:last-of-type').append('<div class="the-step-text"></div>');
            //
          	// 			}
            //
          	// 			$('#recipe-steps-content').find('.the-step:last-of-type .the-step-text').html(string);
          	// 			tinymce.get('mce_step').setContent('');
          	// 			$('p.error').fadeOut();
            //
          	// 		}else{
          	// 			$('p.error.error-txt').fadeIn();
          	// 			return;
          	// 		}
            //
          	// 	});
            //
            //
          	// }


            });
      //   });
      // }
    }
  }
}]);
