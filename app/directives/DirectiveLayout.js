angular.module("DirectiveLayout", [])

.directive('headapp',  ['$rootScope', function($rootScope) {
  return {
    restrict: "E",
    templateUrl:"app/templates/header.html"
  }
}])

.directive('footapp',  ['$rootScope', function($rootScope) {
  return{
    restrict: "E",
    templateUrl:"app/templates/footer.html"
  }
}]);
