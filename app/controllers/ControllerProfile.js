angular.module('ControllerProfile', [])

.controller('profile', function($scope, $http, $location, $cookies, API){
  if($cookies.get('user_id')){

    $scope.success = false;
    $scope.error = false;

    payload = {"fields":{"password":false}}
    $http.get(API+'Users/'+$cookies.get("user_id")+'?filter='+JSON.stringify(payload)).then(function(response){
      $scope.user = response.data;
    });

    $scope.updateUser = function(user){
      var day = $("input[name=BirthDay]").val();
      var month = $("#BirthMonth").val();
      var year = $("input[name=BirthYear]").val();
      var date = year+"-"+month+"-"+day;
      console.log(user);
      //if(username && email ) {
        payload = {"datedenaissance": date,"username": user.username, "email": user.email};
        //payload = {"username": username, "email": username};
      //}
      //console.log(datedenaissance);
      //if(datedenaissance) {
      //  payload["datedenaissance"] = "ok";
      //}
      //
      if( payload ) {
        $http.put(API + 'Users/' + $cookies.get("user_id"), payload);
        $scope.success = !$scope.success;
      }
      //}else{
      //  $scope.success = false;
      //  $scope.error = !$scope.error;
      //}
    }
  }else{
    $location.url('/login')
  }

});
