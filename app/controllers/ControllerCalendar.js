angular.module('ControllerCalendar', [])

.controller('calendar', function($scope, $rootScope, $http, $routeParams, $location, $cookies, API, filterFilter, $window){
  $scope.currentPage = 1;
  $scope.entryLimit = 3;
  $scope.displayedWeek = new Date;

  if($cookies.get("user_id")){
    $scope.curPage = 0;
    $scope.pageSize = 1;

    $scope.mypagesuiv = function(){
      $scope.displayedWeek = $scope.displayedWeek.addDays(7);
      getWeek();
    }

    $scope.mypageprev = function(){
      $scope.displayedWeek = $scope.displayedWeek.addDays(-7);
      getWeek();
    }

    $rootScope.semaine = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];

    $scope.addrepas = function(nom, date, heure){
      payload = {
          "createdAt": date,
          "updatedAt": date,
          "daterepas": date,
          "name":nom,
          "owner": $cookies.get("user_id")
        }
      $http.post(API + 'Repas', payload)
      $window.location.reload();
    }

    $scope.compareDate = function(datebase, datecalendar){
      result = false;
    
      var date = new Date(datebase);
      if(date.toISOString().slice(0, 10) == datecalendar.toISOString().slice(0, 10)){
        result = true
      }
      return result
    }

    $scope.addrecette = function(repas){
      tab = []
      for (var i = 0; i < repas.length; i++){
        tab.push(JSON.parse(repas[i]));
        for (var j = 0; j < tab.length; j++)
          $http.post(API + 'Recetterepas', {"createdAt": tab[j].jour,"updatedAt": tab[j].jour,"ordre": 1,"recette":tab[j].recette ,"repas": tab[j].repas});
          $window.location.reload();
      }
    }

    $scope.verifrepas = function(lerepas, jour){
       var result = false;
       if(lerepas.jour)
       {
       for (var i = 0; i < lerepas.jour.length; i++){
         if (lerepas.jour[i] == jour){
           result = true;
         }
       }
      }
      return result
    }

    $scope.updateRepas = function(id, name, date){
      if(date){
        $http.put(API + 'Repas/'+id, {"name": name, "daterepas": date})
        $window.location.reload();
      }else{
        $http.put(API + 'Repas/'+id, {"name": name})
      }
    }

    $scope.removeRepas = function(id){
      payload = {"where": {"repas": id}}
      console.log(id);
      $http.get(API + 'Recetterepas?filter='+JSON.stringify(payload)).then(function(response){
        if(response.data.length > 0){
          for(i = 0; i < response.data.length; i++){
            $http.delete(API + 'Recetterepas/'+response.data[i].id)
            $http.delete(API + 'Repas/'+id);
          }
        }else {
          $http.delete(API + 'Repas/'+id);
        }
        $window.location.reload();
      });
    }

    $scope.removeRecetteOnrepas = function(id){
      $http.delete(API + 'Recetterepas/'+id);
      $window.location.reload();
    }

    $scope.updateNewRepasDate = function(date){
      var monthString = date.getMonth();

      if( monthString < 10 )
        monthString = "0" + monthString;

      var dayString = date.getDate();

      if( dayString < 10 )
        dayString = "0" + dayString;

      var fullString = dayString + "/" + monthString + "/" + date.getFullYear();

      $scope.newRepasDateString = fullString;
    }

    function getWeek(){
      var month = new Array();
      month[0] = "Janvier";
      month[1] = "Février";
      month[2] = "Mars";
      month[3] = "Avril";
      month[4] = "Mai";
      month[5] = "Juin";
      month[6] = "Juillet";
      month[7] = "Août";
      month[8] = "Septembre";
      month[9] = "Octobre";
      month[10] = "Novembre";
      month[11] = "Décembre";

      $scope.displayedWeekNumber = $scope.displayedWeek.getWeek();
      $scope.displayedYearNumber = $scope.displayedWeek.getFullYear();

      firstDayOfWeek = getDateOfISOWeek($scope.displayedWeekNumber, $scope.displayedYearNumber);

      $scope.displayedDays = [firstDayOfWeek];
      $scope.displayedMonth = month[firstDayOfWeek.getMonth()];

      for(var i = 1; i < 7; i++){
        newDate = getDateOfISOWeek($scope.displayedWeekNumber, $scope.displayedYearNumber).addDays(i);
        $scope.displayedDays.push(newDate);
      }

      var startMonthString = $scope.displayedDays[0].getMonth();

      if( startMonthString < 10 )
        startMonthString = "0" + startMonthString;

      var startDayString = $scope.displayedDays[0].getDate() + 1;

      if( startDayString < 10 )
        startDayString = "0" + startDayString;

      var startFullString = $scope.displayedDays[0].getFullYear() + "-" + startMonthString + "-" + startDayString;

      var endMonthString = $scope.displayedDays[6].getMonth() + 1;

      if( endMonthString < 10 )
        endMonthString = "0" + endMonthString;

      var endDayString = $scope.displayedDays[6].getDate();

      if( endDayString < 10 )
        endDayString = "0" + endDayString;

      var endFullString = $scope.displayedDays[1].getFullYear() + "-" + endMonthString + "-" + endDayString;

      requestFilter = {
        "where": {
          "daterepas":{
            "between":[startFullString, endFullString]
          },
          "owner": $cookies.get("user_id")
        }
      };

      $http.get(API + '/repas?filter='+JSON.stringify(requestFilter))
        .success(function(data){
          if( data ){
            $scope.repas = data;
          }
      });
    }

    function weeksInYear(year) {
      var month = 11, day = 31, week;

      // Find week that 31 Dec is in. If is first week, reduce date until
      // get previous week.
      do {
        d = new Date(year, month, day--);
        week = getWeekNumber(d)[1];
      } while (week == 1);

      return week;
    }

    function getDateOfISOWeek(w, y) {
      var simple = new Date(y, 0, 1 + (w - 1) * 7);
      var dow = simple.getDay();
      var ISOweekStart = simple;
      if (dow <= 4)
          ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
      else
          ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
      return ISOweekStart;
    }

    function getDaysInMonth(month, year) {
         var date = new Date(year, month, 1);
         var days = [];
         while (date.getMonth() === month) {
            days.push(new Date(date));
            date.setDate(date.getDate() + 1);
         }
         return days;
    }

    $rootScope.recetteQuery = "";

    $scope.searchRecette = function(search){
      payload = {"where": {"name": {"regexp": "/"+search+"/i"}}}
      $http.get(API + 'Recettes?filter='+JSON.stringify(payload)).then(function(response){
        $rootScope.recetteQuery = response.data;
      });
    }

    function getFullDaysOfYears(){
      tab = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
      fullDate = []
      var now = new Date();
      for (var i = 0; i < tab.length; i++){
        fullDate.push(getDaysInMonth(tab[i], now.getFullYear()))
        $scope.fullDayOfYear.push(getDaysInMonth(tab[i], now.getFullYear()))
      }
      return fullDate
    }
    $http.get(API + 'recettes').success(function(response){
        $rootScope.recettes = response;
      $http.get(API + 'Etapes').success(function(json) {
        $http.get(API + 'Ingredientrecettes').success(function(result) {
          $http.get(API + 'Ingredients').success(function (resultIngredients) {
            for (var i = 0; i < $rootScope.recettes.length; i++) {
              $rootScope.recettes[i].etapes = [];
              for (var j = 0; j < json.length; j++) {
                if ($rootScope.recettes[i].id == json[j].recette) {
                  $rootScope.recettes[i].etapes.push(json[j])
                }
              }
              $rootScope.recettes[i].ingredients = [];
              for (var k = 0; k < result.length; k++) {
                if ($rootScope.recettes[i].id == result[k].recette) {
                  for (var h=0;h< resultIngredients.length; h++){
                    if (result[k].ingredient == resultIngredients[h].id) {
                      $rootScope.recettes[i].ingredients.push(resultIngredients[h]);
                      $rootScope.recettes[i].ingredients.push({"name":result[k].quantite});
                    }
                  }
                }
              }
            }

            $scope.search = {};
            $scope.resetFilters = function () {
              $scope.search = {};
            };

            $scope.noOfPages = Math.ceil($scope.recettes.length / $scope.entryLimit);

            $scope.$watch('search', function (newVal, oldVal) {
              $scope.filtered = filterFilter($scope.recettes, newVal);
              $scope.noOfPages = Math.ceil($scope.filtered.length / $scope.entryLimit);
              $scope.currentPage = 1;
            }, true);
          });
        });
        });
    });

    $scope.recetteByRepas = []
    $http.get(API + 'Recetterepas').success(function(recetterepas){
      $http.get(API + 'recettes').success(function(recette){
        $http.get(API + 'repas').success(function(repas){
          for (var i = 0; i <recetterepas.length; i++){
            for (var j = 0; j < recette.length; j++){
              for (var k = 0; k < repas.length; k++){
                if(recetterepas[i].repas == repas[k].id && recetterepas[i].recette == recette[j].id){
                  $scope.recetteByRepas.push({"repas": repas[k].id, "recetteName":recette[j].name, "recette_id":recette[j].id, "idRepasRecette":recetterepas[i].id})
                }
              }
            }
          }
        });
      });
    });

    var allRepasRequest = {
      "where": {
        "owner": $cookies.get("user_id")
      },
      "order": 'daterepas DESC',

    }
    $http.get(API + 'repas?filter='+JSON.stringify(allRepasRequest)).success(function(repas){
      allRepas = repas;

      $scope.allRepas = {};

      allRepas.forEach(function(value, index){
        date = new Date( value.daterepas );
        dateString = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();

        if( $scope.allRepas[ dateString ] )
          $scope.allRepas[ dateString ].push( value );
        else
          $scope.allRepas[ dateString ] = [ value ]
      });

    })
    // $http.get(API + 'repas').success(function(response){
    //     $rootScope.repas = response;
    // });

    getWeek();
  }else{
    $location.url('/login');
  }
})
