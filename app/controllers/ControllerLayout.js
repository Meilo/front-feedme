angular.module('ControllerLayout', [])

.controller('layout', function($scope, $rootScope, $http, $routeParams, $location, $cookies){
  $scope.session = $cookies.get('user_id');
  $scope.logout = function(){
    $cookies.remove("user_id");
  }

  $scope.verifCookie = function(session){
    var result = false;
    if (session){
      result = true;
    }
    return result
  }
})
