angular.module('ControllerCategory', [])

.controller('category', function($scope, $rootScope, $http, $routeParams, $location, $cookies, API, filterFilter, $routeParams){
    $http.get(API + 'Categorierecettes').then(function(response){
      $scope.categories = response.data
    })
    $scope.currentPage = 1;
    $scope.entryLimit = 3; // items per page
    if($cookies.get('user_id')){

      $scope.recettes = []
      $http.get(API + 'Recettes').success(function(response){
        $http.get(API + 'Recettecategorierecettes').success(function(json){
          for(var i = 0; i < response.length; i++){
            for(var j = 0; j < json.length; j++){
              if(json[j].recette == response[i].id){
                response[i].categorierecette = json[j].categorierecette
                $scope.recettes =  response
              }
            }
          }
          $scope.search = {};
          $scope.resetFilters = function () {
           	$scope.search = {};
          };

          $scope.noOfPages = Math.ceil($scope.recettes.length / $scope.entryLimit);

            $scope.$watch('search', function (newVal, oldVal) {
         		$scope.filtered = filterFilter($scope.recettes, newVal);
         		$scope.noOfPages = Math.ceil($scope.filtered.length / $scope.entryLimit);
         		$scope.currentPage = 1;
         	}, true);
        })

      })
      $http.get(API + 'Categorierecettes').success(function(response){
        $scope.categorierecettes = response
      })

      $scope.cat = $routeParams.id;

      $scope.verifchecked = function(id){
        if(id == $scope.cat){
          $("#"+id).prop("checked", true);
          result = true
        }
      }

      $scope.colourFilter = function(recettes) {
          var show = true;
          if($("#"+recettes.categorierecette).is(":checked") == false)
          {
          	show = false;
          }
          return show;
      }

      $http.get(API + 'Recettes/'+$routeParams.id).success(function(response){
        $scope.singleRecette = response
        $scope.singleRecette.ingredients = []
        $http.get(API + 'Ingredients').success(function(ingredients){
          $http.get(API + 'ingredientrecettes').success(function(ingredientrecettes){
              for (var j = 0; j < ingredientrecettes.length; j++){
                if (response.id == ingredientrecettes[j].recette)
                {
                    for (var i = 0; i < ingredients.length; i++){
                      if(ingredients[i].id == ingredientrecettes[j].ingredient){
                        ingredients[i].quantite = ingredientrecettes[j].quantite
                        $scope.singleRecette.ingredients.push(ingredients[i])
                      }
                    }
                }
            }
          })
        })
      })
      }else{
        $location.url('/login')
      }
});
