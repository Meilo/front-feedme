angular.module('ControllerRecette', [])

.controller('recette', function($rootScope, $scope, $http, $location, $cookies, API, filterFilter, $routeParams, $timeout){
  $scope.currentPage = 1;
  $scope.entryLimit = 3; // items per page

    $scope.recettes = []
    $http.get(API + 'Recettes').success(function(response){
      $http.get(API + 'Recettecategorierecettes').success(function(json){
        for(var i = 0; i < response.length; i++){
          for(var j = 0; j < json.length; j++){
            if(json[j].recette == response[i].id){
              response[i].categorierecette = json[j].categorierecette
              $scope.recettes =  response
            }
          }
        }
        $scope.search = {};
        $scope.resetFilters = function () {
         	$scope.search = {};
        };

        $scope.noOfPages = Math.ceil($scope.recettes.length / $scope.entryLimit);

          $scope.$watch('search', function (newVal, oldVal) {
       		$scope.filtered = filterFilter($scope.recettes, newVal);
       		$scope.noOfPages = Math.ceil($scope.filtered.length / $scope.entryLimit);
       		$scope.currentPage = 1;
       	}, true);
      })

    })
    $http.get(API + 'Categorierecettes').success(function(response){
      $scope.categorierecettes = response
    })

    $http.get(API + 'Recettes/'+$routeParams.id).success(function(response){
      $scope.thisRecette =  response
    })

    if($rootScope.result){
      $rootScope.result.forEach(function(value){
        console.log(value.split(";")[1]);
        $http.get(API + 'Typequantites/'+value.split(";")[1]).success(function(typequantite){
          $http.get(API + 'Ingredients/'+value.split(";")[2]).success(function(ingredient){
            $rootScope.quantiteinfo = {"name": typequantite.name, "quantite": value.split(";")[0], "ingredient": ingredient.name, "idIngredient": ingredient.id}
          })
        })
      });
    }

    $scope.addfinalRecette = function(){
      payload = {
        "author": $cookies.get('user_id'),
        "image": "string",
        "name": $rootScope.name_recette,
        "description": $rootScope.description_recette
      }
      $http.post(API + 'Recettes', payload).then(function(){
        data = {"where":{"name":$rootScope.name_recette}}

        $http.get(API + 'Recettes?filter='+JSON.stringify(data)).then(function(response){
          post_cat = {
            "categorierecette": $rootScope.data_recette[0],
            "recette": response.data[0].id
          }
          $http.post(API + 'Recettecategorierecettes', post_cat).then(function(){
            $rootScope.result.forEach(function(value){
              post_in = {
                "ingredient": value.split(";")[2],
                "quantite": value.split(";")[0],
                "recette": response.data[0].id
              }
              $http.post(API + 'Ingredientrecettes', post_in)
              post_et = {
                  "description": $rootScope.etapes[0],
                  "numeroetape": 1,
                  "recette": response.data[0].id,
                  "titre": "Etape",
                }
              $http.post(API + 'Etapes', post_et)
            });
          })
        })
      })

    }

    $http.get(API + 'Ingredients').success(function(ingredients){
      $http.get(API + 'Ingredienttypequantites').success(function(ingredienttypequantite){
        $http.get(API + 'Typequantites').success(function(typequantite){
          $scope.Ingredients = ingredients
          for(var i = 0; i < $scope.Ingredients.length; i++){
            for(var j = 0; j < ingredienttypequantite.length; j++){
              for(var k = 0; k < typequantite.length; k++){
                if($scope.Ingredients[i].id == ingredienttypequantite[j].ingredient){
                  if(typequantite[k].id == ingredienttypequantite[j].typequantite){
                    $scope.Ingredients[i].typequantite = []
                    $scope.Ingredients[i].typequantite.push(typequantite[k])
                  }
                }
              }
            }
          }
        })
      })
    })

    $http.get(API + 'categorieingredients').success(function(response){
      $scope.categorieingredients = response
    })

    $scope.idRecette = $routeParams.id;

    $scope.colourFilter = function(recettes) {
        var show = true;
        if($("#"+recettes.categorierecette).is(":checked") == false)
        {
        	show = false;
        }
        return show;
    }





    console.log($rootScope)
    $scope.addIngredient = function(ingredients){
      // for(var i = 0; i< tab.length; i++){
        // payload = {"quantite_style": ingredients[i].split(';')[0], "idIngredient": ingredients[i].split(';')[1], "quantite":ingredients[i].split(';')[0].split(' ')[0]}
      $rootScope.result = ingredients
      // }
      // for(var i=0; i < ingredients.length; i++){
      //   payload = {"createdAt": "2016-07-09","updatedAt": "2016-07-09","ingredient": ingredients[i],"quantite": 2,"recette": $routeParams.id}
      //   $http.post(API + "Ingredientrecettes", payload)
      // }
    }

    // if($cookies.get("etapes") && $cookies.get('name_recette') && $cookies.get('description_recette') && $cookies.get('data')){
    //   $scope.etapes = $cookies.get("etapes").split(",");
    //   $scope.name_recette = $cookies.get('name_recette');
    //   $scope.description_recette = $cookies.get('description_recette');
    //   $scope.data_recette = $cookies.get('data');
    // }

    $rootScope.tab = []
    $scope.addEtape = function(etapes){
      $rootScope.tab.push(etapes)
      $rootScope.etapes = $rootScope.tab

      string = $rootScope.etapes;
      console.log(string);

      if(string != ''){
  			if( $('#upload-step-image').val() != ''  ){

  				var val = $('#upload-step-image').val();
  				var valTest = val.substring(val.lastIndexOf('.') + 1).toLowerCase();
  				var isImage = false;
  				if( valTest == 'gif' || valTest ==  'jpg' || valTest ==  'png' ) isImage = true;

  				if(isImage){

  					$('#recipe-steps-content').append('<div class="the-step clearfix"></div>');
  					$('#recipe-steps-content').find('.the-step:last-of-type').append('<div class="the-step-image"></div><div class="the-step-text"></div>');
  					val = './img/poulet-salade.jpg';
  					$('#upload-step-image').val('');
  					$('#recipe-steps-content').find('.the-step:last-of-type .the-step-image').html('<img src="' + val + '">');

  				}else{
  					$('p.error.error-image').fadeIn();
  					return;
  				}

  			}else{

  				$('#recipe-steps-content').append('<div class="the-step clearfix"></div>');
  				$('#recipe-steps-content').find('.the-step:last-of-type').append('<div class="the-step-text"></div>');

  			}

  			$('#recipe-steps-content').find('.the-step:last-of-type .the-step-text').html(string);
  			//tinymce.get('mce_step').setContent('');
  			$('p.error').fadeOut();

  		}else{
  			$('p.error.error-txt').fadeIn();
  			return;
  		}
      // $http.post(API + 'Etapes', {"createdAt": "2016-07-09","description":etapes, "numeroetape": 1,"recette": $routeParams.id ,"thumbnail": "string","titre": "Etape", "updatedAt": "2016-07-09"})
    }

    $scope.ingreFilter = function(ingredient) {
        var show = true;
        if($("#"+ingredient.categorie).is(":selected") == false)
        {
        	show = false;
        }
        return show;
    }
    // $scope.biam = []
    // $http.get(API + 'Ingredients').success(function(response){
    //   $http.get(API + 'Ingredientrecettes').success(function(json){
    //     for(var i = 0; i < $scope.result.split(",").length; i++){
    //       for(var j = 0; j < response.length; j++){
    //         if($scope.result.split(",")[i] == response[j].id){
    //           $scope.biam.push(response[j])
    //         }
    //       }
    //     }
    //   })
    // })

    $http.get(API + 'Recettes/'+$routeParams.id).success(function(response){
      $scope.singleRecette = response
      $scope.singleRecette.ingredients = []
      $scope.singleRecette.etapes = []
      $http.get(API + 'Ingredients').success(function(ingredients){
        $http.get(API + 'ingredientrecettes').success(function(ingredientrecettes){
          $http.get(API + 'Etapes').success(function(etapes){
            for (var j = 0; j < ingredientrecettes.length; j++){
              if (response.id == ingredientrecettes[j].recette)
              {
                  for (var i = 0; i < ingredients.length; i++){
                    if(ingredients[i].id == ingredientrecettes[j].ingredient){

                      ingredients[i].quantite = ingredientrecettes[j].quantite
                      $scope.singleRecette.ingredients.push(ingredients[i])
                    }
                  }
              }
          }
          for (var k = 0; k < etapes.length; k++){
            if(etapes[k].recette == response.id){
              $scope.singleRecette.etapes.push(etapes[k])
            }
          }
          });
        })
      })
    })



    $scope.onestep =  function(name, description, data){
      $rootScope.name_recette = name;
      $rootScope.description_recette = description;
      $rootScope.data_recette = data;
      // payload = {"author": $cookies.get('user_id'), "createAt": "2016-07-09", "image": "string", "name": name, "updatedAt": "2016-07-09", "description": description}
      // $http.post(API + 'Recettes', payload).then(function(response){
      //   for(var i = 0; i < data.length; i++){
      //     $http.post(API + 'Recettecategorierecettes', {"categorierecette":data[i], "createAt": "2016-07-09", "updatedAt": "2016-07-09", "recette":response.data.id})
      //   }
      //   $location.url('/recette/create/two/'+response.data.id);
      // })
    }

    $scope.onestepUpdate = function(name, description, data){
      payload = {"author": $cookies.get('user_id'), "createAt": "2016-07-09", "image": "string", "name": name, "updatedAt": "2016-07-09", "description": description}
      $http.put(API + 'Recettes/'+$routeParams.id, payload).then(function(response){
        if(data.length < 0){
          $http.get(API + 'Recettecategorierecettes').success(function(json){
            for(var i = 0; i < data.length; i++){
              for(var j = 0; j < json.length; j++){
              if(data[i].id != json[j].id){
                $http.put(API + 'Recettecategorierecettes', {"categorierecette":data[i], "createAt": "2016-07-09", "updatedAt": "2016-07-09", "recette":response.data.id})
              }
            }
            }
          })
        }
        $location.url('/recette/create/two/'+response.data.id);
      })
    }

    $scope.addIngredientUpdate = function(ingredients){
      $http.get(API + "Ingredientrecettes").success(function(response){
        if(ingredients.length < 0){
          for(var i=0; i < ingredients.length; i++){
            for(var j=0; j < response.length; j++){
              console.log(response)
              if(ingredients[i] != response[j].ingredient)
              {
                payload = {"createdAt": "2016-07-09","updatedAt": "2016-07-09","ingredient": ingredients[i],"quantite": 2,"recette": $routeParams.id}
                //console.log(payload)
                //$http.post(API + "Ingredientrecettes", payload)
              }
            }
          }
        }
      })

    }



});
