angular.module('ControllerLogin', [])

.controller('login', function($scope, $http, $location, $cookies, API){

  $scope.login = function(username, password){
    payload = {"where": {"username": username, "password": password}};
    $http.get(API+'Users?filter='+JSON.stringify(payload)).then(function(response){
        $cookies.put("user_id", response.data[0].id);
        $location.url('/calendar');
      });
  }

  $scope.register = function(username, password, repassword){
    if(password == repassword){
      payload = {"where": {"username": username, "password": password}};
      $http.get(API+'Users?filter='+JSON.stringify(payload)).then(function(response){
          if(response.data.length == 0){
            var payload_two = {"username":username, "password":password, "etatcompte":0, "role":0, "email":username};
            $http.post(API+'Users', payload_two).then(function(){
              $http.get(API+'Users?filter='+JSON.stringify(payload)).then(function(json){
                $cookies.put("user_id", json.data[0].id);
                $location.url('/calendar');
              });
            });
          }
      });
    }
  }

})
