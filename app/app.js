var app = angular.module("feedme", [
  "ngRoute",
  "ui.bootstrap",
  "angularCSS",
  "ngCookies",
  "ngResource",
  "ControllerCalendar",
  "DirectiveCalendar",
  "FilterCalendar",
  "ControllerHome",
  "ControllerLayout",
  "DirectiveLayout",
  "ControllerLogin",
  "DirectiveLogin",
  "ControllerProfile",
  "ControllerCategory",
  "ControllerRecette",
  "DirectiveRecette",
  "FilterRecette",
  "ui.tinymce",
  "ngSanitize"
]);

app.config(['$routeProvider', function($routeProvider){

  //routes system
  $routeProvider
  .when('/calendar', {
    title:'Calendar - FeedMe',
    templateUrl: 'app/templates/calendar.html',
    controller: 'calendar'
  })
  .when('/recette', {
    title:'Recette - FeedMe',
    templateUrl: 'app/templates/recettes.html',
    controller: 'recette'
  })
  .when('/recette/create/one', {
    title:'Recette - FeedMe',
    templateUrl: 'app/templates/createRecetteOne.html',
    controller: 'recette'
  })
  .when('/recette/create/two', {
    title:'Recette - FeedMe',
    templateUrl: 'app/templates/createRecetteTwo.html',
    controller: 'recette'
  })
  .when('/recette/create/four/', {
    title:'Recette - FeedMe',
    templateUrl: 'app/templates/createRecetteFour.html',
    controller: 'recette'
  })
  .when('/recette/create/three/', {
    title:'Recette - FeedMe',
    templateUrl: 'app/templates/createRecettethree.html',
    controller: 'recette'
  })
  .when('/recette/create/update/one/:id', {
    title:'Recette - FeedMe',
    templateUrl: 'app/templates/createRecetteOneUpdate.html',
    controller: 'recette'
  })
  .when('/recette/create/update/two/:id', {
    title:'Recette - FeedMe',
    templateUrl: 'app/templates/createRecetteTwoUpdate.html',
    controller: 'recette'
  })
  .when('/recette/create/update/four/', {
    title:'Recette - FeedMe',
    templateUrl: 'app/templates/createRecetteFourUpdate.html',
    controller: 'recette'
  })
  .when('/recette/create/update/three/:id', {
    title:'Recette - FeedMe',
    templateUrl: 'app/templates/createRecettethreeUpdate.html',
    controller: 'recette'
  })
  .when('/recette/:id', {
    title:'Recette - FeedMe',
    templateUrl: 'app/templates/singleRecette.html',
    controller: 'recette',
    css: 'www/css/login_style.css'
  })
  .when('/category', {
    title:'Category - FeedMe',
    templateUrl: 'app/templates/category.html',
    controller: 'category',
    css: 'www/css/login_style.css'
  })
  .when('/category/recettes/:id', {
    title:'Category - FeedMe',
    templateUrl: 'app/templates/recettesByCategory.html',
    controller: 'category',
    css: 'www/css/login_style.css'
  })
  .when('/login', {
    title:'Login - FeedMe',
    templateUrl: 'app/templates/login.html',
    controller: 'login',
    css: ['www/css/reset.css','www/css/login_style.css']
  })
  .when('/register', {
    title:'Register - FeedMe',
    templateUrl: 'app/templates/register.html',
    controller: 'login',
    css: ['www/css/reset.css','www/css/login_style.css']
  })
  .when('/lostpassword', {
    title:'Lost - FeedMe',
    templateUrl: 'app/templates/lost-password.html',
    controller: 'login',
    css: ['www/css/reset.css','www/css/login_style.css']
  })
  .when('/logout', {
    controller: 'layout',
    redirectTo: '/'
  })
  .when('/profile', {
    title:'Profile - FeedMe',
    templateUrl: 'app/templates/profile.html',
    controller: 'profile',
    css: 'www/css/myaccount.css'
  })
  .when('/', {
    title:'Home - FeedMe',
    templateUrl: 'app/templates/home.html',
    controller: 'home'
  })
  .otherwise({
    redirectTo: '/'
  });

}]);

// chargement du title de la page dynamiquement
app.run(['$rootScope', function($rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.title = current.$$route.title;
      });
}]);

app.constant('API', 'http://dev.guillaume-trinh.fr/api/');
