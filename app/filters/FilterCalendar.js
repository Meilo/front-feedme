angular.module('FilterCalendar', [])

.filter('convertDate', function () {
	return function (date) {
		var weekday = new Array(7);
		weekday[0]=  "Dimanche";
		weekday[1] = "Lundi";
		weekday[2] = "Mardi";
		weekday[3] = "Mercredi";
		weekday[4] = "Jeudi";
		weekday[5] = "Vendredi";
		weekday[6] = "Samedi";
		return weekday[date.getDay()] + " " + date.getDate();
	};
})

.filter('convertDate2', function () {
	return function (date) {
		date = new Date(date);
		var weekday = new Array(7);
			weekday[0]=  "Dimanche";
			weekday[1] = "Lundi";
			weekday[2] = "Mardi";
			weekday[3] = "Mercredi";
			weekday[4] = "Jeudi";
			weekday[5] = "Vendredi";
			weekday[6] = "Samedi";

		var month = new Array();
		    month[0] = "Janvier";
		    month[1] = "Février";
		    month[2] = "Mars";
		    month[3] = "Avril";
		    month[4] = "Mai";
		    month[5] = "Juin";
		    month[6] = "Juillet";
		    month[7] = "Août";
		    month[8] = "Septembre";
		    month[9] = "Octobre";
		    month[10] = "Novembre";
		    month[11] = "Décembre";

		return weekday[date.getDay()] + " " + date.getDate() + " " + month[date.getMonth()+1] + " " + date.getFullYear();
	};
})
