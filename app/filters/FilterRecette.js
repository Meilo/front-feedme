angular.module('FilterRecette', [])

.filter('startFrom', function () {
 return function (input, start) {
   if (input) {
     start = +start;
     return input.slice(start);
   }
   return [];
  }
})

// .filter('filterCategorieRecette', function () {
//     return function (recettes) {
//         return recettes.filter(function (d) {
//           var show = true;
//           if($("#"+recettes.categorierecette).is(":checked") == false)
//           {
//             //console.log(recettes.categorierecette)
//           	show = false;
//           }
//           return show;
//         });
//     };
// })
