$(document).ready(function() {
    $('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = $(this).attr('href');

        // Show/Hide Tabs
        $('.tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        $(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
});

$(window).load(function(){
	if( $('#result-slideshow').length){
		$('#result-slideshow').bxSlider({
			pager: false,
			slideWidth: 300,
			minSlides: 3,
			maxSlides: 3
		});
	}
});
